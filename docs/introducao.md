# Introdução
O “Tracker solar”, é um módulo fotovoltaico com sistema de rastreamento que acompanha o movimento do sol, foi pensado com o intuito de aumentar a eficiência da geração de energia da placa solar fotovoltaica, a ser desenvolvido com conhecimentos multidisciplinares, das Engenharias Aeroespacial, Eletrônica, de Energia e de Software.

## 1.1. Detalhamento do problema

A crescente inserção de fontes de energia renováveis nas matrizes energéticas, impulsionada pela necessidade de mitigação de impactos ambientais e climáticos, caminha juntamente com o desenvolvimento de novas tecnologias e inovações.

Nesse contexto, a energia solar fotovoltaica é uma das fontes de energia renovável que vem sendo explorada e cada vez mais utilizada. Esta tecnologia transforma a luz solar em eletricidade por meio de células fotovoltaicas, oferecendo uma fonte de energia abundante e de baixa emissão de carbono. Apesar disto, um dos desafios enfrentados neste tipo de tecnologia é a sua baixa eficiência.

Surge, então, a ideia do “Tracker solar”, um dispositivo mecânico e eletrônico projetado para orientar as placas solares fotovoltaicas, através do rastreamento do movimento do sol durante o dia.

O “Tracker solar” contará com:

- Estrutura metálica - Garante a sutentação dos paineis fotovoltaicos e permite que ele rotacione em torno de um eixo paralelo ao chão.
- Motor de passo - Permite o posicionamento preciso dos paineis fotovoltaicos através da rotação em torno do eixo.
- Sensores de luminosidade - Utilizados para a determinação da posição do sol no céu. Estes sensores convertem a luz em sinais elétricos proporcionais à luz incidente, que, posteriormente, são processados para detectar a direção do sol.
- Sensores de posição - Permitem o acompanhamento em tempo real da posição do conjunto de placas e do possível deslocamento delas.
- Sistema de controle - Serve para receber os sinais enviados pelos sensores de luminosidade e determinar os movimentos necessários para manter os paineis fotovoltaicos alinhados com a trajetória do sol.
- Integração das APIs - O software tem a integração com as APIs, fornecendo dados da estação solarimétrica em tempo real.
- Software de monitoramento - Um painel virtual que mostra gráficos com os dados necessários para fazer o monitoramento do "Tracker solar". Fazendo o monitoramento em tempo real e o histórico dos dados de desempenho do sistema.
- Comunicação remota - O software terá a capacidade de comunicação remota, garantindo o controle e monitoramento do sistema a partir de qualquer localização.
- Fluxo de exceção - emite notificações de alerta para o usuário sempre que ocorrer um problema específico ou potencial problema.
- Fonte de energia - Para manter todo o sistema do “Tracker Solar” em pleno funcionamento, ele deverá ter uma fonte de alimentação. Sendo um sistema “On-Grid”, será alimentado pela própria rede elétrica já instalada no local.

## 1.2. Levantamento de normas técnicas relacionadas ao problema

Para o desenvolvimento do projeto, houve um levantamento de normas técnicas, de todas as áreas de conhecimento, para embasamento e a correta execução para as etapas do projeto, tanto para a concepção e planejamento quanto para a execução e avaliação do projeto. A normas possibilitam a padronização testada e garante segurança, eficácia e eficiência das ações.

As seguintes normas foram averiguadas:

- **ABNT NBR 5410** - Instalações elétricas de baixa tensão: Estabelece parâmetros para instalações elétricas de baixa tensão, objetivando a
  segurança de pessoas e animais, o funcionamento da instalação e conservação de bens (ABNT, 2004, p. 1).
  Útil para garantir tanto a segurança de alunos, professores e demais pessoas que interajam com o sistema de tracker solar quanto para garantir
  seu correto funcionamento relativo às instalações elétricas.
- **ABNT NBR 14136** - Plugues e tomadas para uso doméstico e análogo: Fixa as dimensões de plugues e tomadas de características nominais 
  até 20A / 250V em corrente alternada, para uso doméstico com ligação a sistemas de distribuição com tensões compreendidas entre 100 V 
  e 250 V em corrente alternadas (ABNT, 2012, p. 1). Utilizada no projeto para o planajamento e montagem do circuito alimentado pela placa solar.
- **ISO 9060** - Energia solar: Especificação e classificação de instrumentos para medição solar hemisférica e radiação solar direta: Estabelece uma categorização 
  e descrição de dispositivos para medição da radiação solar hemisférica e direta, integradas dentro de uma faixa espectral que varia de 0,3 μm a 4 μm. A classificação 
  dos instrumentos é baseada nos resultados obtidos a partir de testes de desempenho (ISO, 2018). O padrão foi utilizado para auxiliar na aferição de dados 
  solarimétricos através de instrumentos como o piranômetro.
- **ABNT NBR 16274** - Sistemas Fotovoltaicos Conectados à Rede de distribuição: define os requisitos mínimos de informação e documentação a serem registrados após a 
  conclusão da instalação de um sistema fotovoltaico conectado à rede. Além disso, aborda a descrição da documentação, os testes de comissionamento e os critérios de 
  inspeção para garantir a segurança e o funcionamento adequado do sistema instalado (ABNT, 2014). Serve para o projeto para avaliação do funcionamento do sistema fotovoltaico
  e para que se possa inspecioná-lo.
- **ABNT NBR 16690:2019** – Instalações elétricas de arranjos fotovoltaicos – Requisitos de projeto: Estabelece os critérios de projeto para as instalações elétricas 
  de arranjos fotovoltaicos, abrangendo aspectos como condutores, dispositivos de proteção elétrica, dispositivos de manobra, aterramento e equipotencialização. O 
  escopo desta norma abrange todas as partes do arranjo fotovoltaico, exceto os dispositivos de armazenamento de energia, unidades de condicionamento de potência 
  ou as cargas(ABNT, 2019).
- **ABNT NBR 5419-1:2015** – Proteção contra descargas atmosféricas: requisitos para a determinação de proteção contradescargas atmosféricas. (ABNT, 2015). Útil 
  pois a montagem do circuito ao ar-livre o torna sujeito a descargas atmosféricas.
- **ABNT NBR 13712:1996** - Luvas de proteção: Esta Norma estabelece os princípios gerais para a padronização de luvas de proteção confeccionadas 
  em couro ou tecido (ABNT, 1996). Para a escolha e uso correto de luvas no manuseio de equipamentos cortantes e como serrar e de outros equipamentos de oficinas 
  como ferros de solda.
- **ABNT NBR ISO 20345:2008** - Equipamento de proteção individual - Calçado de segurança: Esta Norma especifica os requisitos básicos e opcionais
  para os calçados de segurança (ABNT, 2008). Para a escolha e uso correto de calçados de segurança no manuseio de equipamentos das oficinas mecânicas.
- **ABNT NBR 16247:2013** - Proteção ocular pessoal — Filtros para soldagem e técnicas associadas: Requisitos de transmitância e recomendações de 
  uso. Especifica os números de escala e os requisitos de transmitância para filtros, para proteger operadores que realizam trabalhos envolvendo 
  soldagem, brasagem, goivagem (abertura de sulcos) e cortes a plasma. (ABNT, 2013). Servirá para noções de escolha e uso de proteção ocular durante o 
  manuseio de equipamentos como serras de mão e ferros de solda.
- **NR 10** - Segurança em instalações e serviços em eletricidade: estabelece os requisitos e condições mínimas objetivando a implementação de medidas 
  de controle e sistemas preventivos, de forma a garantir a segurança e a saúde dos trabalhadores que, direta ou indiretamente, interajam em instalações elétricas 
  e serviços com eletricidade (BRASIL, 1978).
- **ISO/IEC 25010:2023** - Sistemas e Engenharia de *Software* - Requisitos de Qualidade e Avaliação (SQuaRE): estabelece um modelo de qualidade para produtos de 
  tecnologia da informação e comunicação (TIC) e produtos de software, composto por nove características e suas subcategorias, que servem como referência para
  especificação, medição e avaliação da qualidade. Ele pode ser utilizado por diversos interessados, como desenvolvedores, compradores e avaliadores 
  independentes, ao longo do ciclo de vida do produto (ISO, 2023). É útil para determinação, avaliação e manutenção da qualidade dos produtos de Software desenvolvidos no 
  projeto e determinação e gerenciamento dos requisitos funcionais e não funcionais da solução de *software*.
- **ISO/IEC/IEEE 29148:2018** - Sistemas e Engenharia de *Software* — Processos de ciclo de vida — Engenharia de requisitos:
- **ISO/IEC 27001:2022** - Segurança da informação, cibersegurança e proteção de privacidade - Sistemas de gerenciamento de segurança da informação:
- **ABNT NBR 8800:2008** - Projeto de estruturas de aço e de estruturas mistas de aço e concreto de edifícios: Estabelece os requisitos básicos que que devem ser obedecidos no projeto à temperatura ambiente de estruturas de aço e de estruturas mistas de aço e concreto de edifícios (ABNT, 2008, p. 1). Essencial para a concepção da estrutura.
- **ABNT NBR 12 - Estabelece a aplicação de máquinas e dispositivos caracterizados por proteções fixas, móveis e dispositivos de segurança interligados ou não, que garantem a saúde e segurança.


## 1.3. Identificação de soluções comerciais

O mercado de seguidores solares teve uma crescente nos últimos anos. Gerou um valor de US$ 7,88 bilhões em 2023, projetando-se para atingir US$ 25,24 bilhões até 2032 (SOLAR, 2023).

Em longo prazo, o mercado de seguidores deve ter uma demanda maior com a crescente instalação de plantas de Energia solar fotovoltáicas, já que os sistemas atuais aumentam a capacidade de geração da placa para valores entre 20 - 30% (INTELLIGENCE, 2023).

No mercado global, a região da américa do norte tem a maior parte do mercado e da demanda, e os Estados Unidos lideram o mercado global (SOLAR, 2023).

Durante a fase inicial do projeto foi feita uma pesquisa de mercado de produtos já existentes, a fim de definir melhor nossos requisitos e entender melhor o seu funcionamento. Foram priorizadas para a pesquisa, soluções que possuíssem especificações técnicas bem definidas.

NexTracker Inc., Array Technologies Inc., PV Hardware Solutions S.L.U., Arctech Solar Holding Co. Ltd e Soltec Power Holdings SA são alguns dos principais participantes do mercado (INTELLIGENCE, 2023).

A tabela a seguir faz uma breve comparação entre as soluções comerciais já existentes (duas dos Estados Unidos e uma solução chinesa) e a proposição do nosso projeto.


**Tabela X -** Detalhamento de produtos existentes no mercado. Os graus de liberdade podem ser um, para um único eixo, ou dois para seguidores com 2 eixos.

|Nome|Empresa|Angulação|Material|Graus de liberdade|Algoritmo|
|---|---|---|---|---|---|
|NX Horizon[^1]|NextTracker Inc.|$$ \pm 60° $$|Aço galvanizado|1 grau|Algoritmo astronômico com padrão de backtracking|
|Utility Dual Track[^2]|Sun Action Trackers|Rotação: 135° Azimut, Vertical: 60°|Aço galvanizado|2|Sensor em tempo real|
|Vanguard - 2P[^3]|TrinaTracker|110°|Aço de alto rendimento|1|*Smart Tracking Algorithm* que utiliza dados em tempo real, imagens topográficas capturadas por drones e modelagem do terreno.|

Fonte: (Autores).


Boa parte das soluções se aplica para plantas de grande porte, com eixos que comportam múltiplas placas, diferentemente da solução que é apresentada pelo presente trabalho, que visa o controle de uma única placa.


[^1]: Mais informações: [https://www.nextracker.com/nx-horizon-solar-tracker](https://www.nextracker.com/nx-horizon-solar-tracker)

[^2]: Mais informações: [https://sat-energy.com/recources/](https://sat-energy.com/recources/)

[^3]: Mais informações: [https://www.trinasolar.com/en-glb/trinatracker/vanguard](https://www.trinasolar.com/en-glb/trinatracker/vanguard)




## 1.4. Objetivo geral do projeto
O projeto tem como principal objetivo maximizar o potencial da energia solar fotovoltaica, a partir do desenvolvimento de um produto que otimize a captação da radiação solar, aumentando a eficiência da geração de energia elétrica.

## 1.5. Objetivos específicos do projeto em cada área


## Objetivo específico de Estrutura

- Construir um produto em que a produção de energia elétrica seja mais eficiente do que num módulo fotovoltaico fixo
- Desenvolver uma estrutura física que atenda aos padrões de segurança
- Elaborar os desenhos mecânicos e da estrutura do projeto com o uso do *software* escolhido pela equipe de estrutura.
- Encontrar soluções comerciais ou já existentes para o problema do projeto
- Entendimento do problema do projeto pelos membros da equipe
- Estimativa do Orçamento a ser usado no projeto
- Estimativa dos Riscos esperados
- Integração das 5 engenharias dentro do projeto 
- Verificação das normas técnicas a respeito do projeto
- Estimativa dos Riscos esperados
- Estimativa do Orçamento a ser usado no projeto
- Registro do desenvolvimento e dos passos realizados durante as aulas por meio das ATAS
- Seleção dos materiais, programas, equipamentos e máquinas a serem utilizados ao longo do projeto 
- Realizar a coleta bibliográfica
- Escolha do Coordenador Geral, Diretor de Qualidade e dos Diretores Técnicos dentro dos membros da equipe 
- Encontrar soluções comerciais ou já existentes para o problema do projeto 
- Entendimento do problema do projeto pelos membros da equipe

## Objetivo específico de Software

- Criar um *software* que possibilite a configuração remota do equipamento pela internet
- Construir um produto em que a produção de energia elétrica seja mais eficiente do que num módulo fotovoltaico fixo
- Desenvolver algoritmos que garantam o posicionamento preciso dos painéis fotovoltaicos ao longo do dia
- Desenvolver uma aplicação *web* com uma interface amigável de visualização de dados e de controle do "Tracker solar" em tempo real.
- Realizar a coleta bibliográfica
- Entendimento do problema do projeto pelos membros da equipe
- Estimativa dos Riscos esperados
- Verificação das normas técnicas a respeito do projeto
- Estimativa do Orçamento a ser usado no projeto
- Registro do desenvolvimento e dos passos realizados durante as aulas por meio das ATAS
- Seleção dos materiais, programas, equipamentos e máquinas a serem utilizados ao longo do projeto 
- Integração das 5 engenharias dentro do projeto 


## Objetivo específico de Eletrônica
- Entendimento do problema do projeto pelos membros da equipe
- Elaborar os diagramas eletrônicos do projeto com o uso do *software* escolhido pela equipe de energia e eletrônica.
- Estimativa dos Riscos esperados
- Garantir o pleno funcionamento de todo o sistema com uma fonte de alimentação segura
- Implementar um sistema de controle que consiga efetuar os movimentos corretos das placas fotovoltaicas de acordo com os dados solarimétricos coletados pelos sensores internos e externos.
- Integrar sensores de luminosidade eficazes e confiáveis.
- Verificação das normas técnicas a respeito do projeto
- Realizar a coleta bibliográfica
- Estimativa do Orçamento a ser usado no projeto
- Registro do desenvolvimento e dos passos realizados durante as aulas por meio das ATAS
- Seleção dos materiais, programas, equipamentos e máquinas a serem utilizados ao longo do projeto 
- Integração das 5 engenharias dentro do projeto 


## Objetivo específico de Energia
- Entendimento do problema do projeto pelos membros da equipe
- Estimativa dos Riscos esperados
- Construir um produto em que a produção de energia elétrica seja mais eficiente do que num módulo fotovoltaico fixo
- Garantir o pleno funcionamento de todo o sistema com uma fonte de alimentação segura
- Elaborar os diagramas elétricos do projeto com o uso do *software* escolhido pela equipe de energia e eletrônica.
- Realizar a coleta bibliográfica
- Estimativa do Orçamento a ser usado no projeto
- Registro do desenvolvimento e dos passos realizados durante as aulas por meio das ATAS
- Seleção dos materiais, programas, equipamentos e máquinas a serem utilizados ao longo do projeto 
- Integração das 5 engenharias dentro do projeto 




# **Apêndice 06 - Memorial de cálculo de elementos do projeto**

Este apêndice pode ser utilizado para detalhar as decisões e resultados
de testes relacionados ao desenvolvimento de software da solução.

Deverá ser feito uma subseção para cada elemento de software

# **Detalhar o projeto do elemento 01 do subsistema 01**

Detalhamento do projeto.

# **Detalhar o projeto do elemento 02 do subsistema 01**

Detalhamento do projeto.

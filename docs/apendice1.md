
 
# **Apêndice 01 - Aspectos de gerenciamento do projeto**

## **Termo de abertura do projeto**

É o documento que autoriza formalmente o projeto. Ele concede ao gerente
a autoridade para utilizar os recursos da organização na execução das
atividades do projeto.

O termo de abertura do projeto deve abordar, ou referenciar, as
seguintes questões:

-   requisitos que satisfazem as necessidades do cliente

-   objetivos do projeto

-   propósito ou justificação do projeto

-   stakeholders do projeto e os seus papéis e responsabilidades

-   expectativas dos stakeholders

-   identificação do gestor do projeto, e nível de autoridade do gerente

-   cronograma macro dos marcos do projeto

-   premissas, ou pressupostos, organizacionais (fatores considerados
    verdadeiros, reais ou certos)

-   restrições organizacionais (fatores que limitam as opções da equipe)

-   investimento (orçamento preliminar)

-   restrições e riscos

-   descrição do(s) subproduto(s) identificado(s)

-   milestones identificados

-   Permite assim responder a questões como:

-   O que deve ser feito para atingir o objetivo do projeto?

-   Como deve ser feito?

-   Quem que vai fazer?

-   Quando deve ser feito?

## **Lista É / Não É**

Relação do que o produto, ou subproduto, é, e do que o produto, ou
subproduto, não é.

Este processo é necessário para restringir ao seu mínimo o escopo do
projeto, garantindo um melhor foco.

## **Organização da equipe**

&emsp; A equipe de desenvolvimento está organizada em três áreas de especialização para maximizar a eficiência e a qualidade do trabalho. Cada área tem um líder dedicado que supervisiona as operações e garante a realização de metas e prazos. A estrutura organizacional visa promover a colaboração entre as equipes.

- Software: liderada pelo João Paulo e contendo 4 desenvolvedores
- Energia/Eletrônica: liderada pelo Jorge Guilherme e contendo 3 desenvolvedores
- Estrutura: liderada por Camila Borba contendo apenas 1 desenvolvedor

&emsp; Além disso, Yngrid Karine é a coordenadora geral da equipe, proporcionando liderança e suporte em todos os aspectos do projeto. José Joaquim é o diretor de qualidade, garantindo que todos os padrões e procedimentos sejam rigorosamente seguidos para alcançar um produto final de excelência.

<figure markdown="span">
  <figcaption>Organograma da equipe de desenvolvimento</figcaption>
    ![Organograma da equipe de desenvolvimento](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/organograma.jpg){ width="800px" }
  <figcaption>Fonte: Autores</figcaption>
</figure>


## **Repositórios**

> Apresentar links para acesso aos repositórios do projeto.

## **EAP (Estrutura Analítica de Projeto) Geral do Projeto**

É com base na técnica de decomposição que se consegue dividir os
principais produtos do projeto em nível de detalhe suficiente para
auxiliar a equipe de gerenciamento do projeto na definição das
atividades do projeto.

### **EAP do subsistema 01**

Inserir a EAP de cada um dos subsistemas que compõe o projeto.

### **EAP do subsistema 02**

Inserir a EAP do subsistema 02.

### **Definição de atividades e backlog das sprints**

Definir as atividades gerais que compõe o projeto e elaborar o
cronograma básico de execução. É importante identificar os responsáveis
pelas atividades.
